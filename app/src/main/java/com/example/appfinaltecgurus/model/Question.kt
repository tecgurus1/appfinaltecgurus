package com.example.appfinaltecgurus.model

data class Question(var id: Int, var question: String, var type: String, var answer: String)