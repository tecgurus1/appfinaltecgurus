package com.example.appfinaltecgurus.ui.gallery

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appfinaltecgurus.Main2Activity
import com.example.appfinaltecgurus.R
import com.example.appfinaltecgurus.adapter.PhotosHolder
import com.example.appfinaltecgurus.dialog.MyProgressDialog
import com.example.appfinaltecgurus.dialog.MyToast
import com.example.appfinaltecgurus.model.Photo
import com.example.appfinaltecgurus.util.FileUtil
import com.example.appfinaltecgurus.util.ImageUtil
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageMetadata
import java.io.ByteArrayOutputStream
import java.io.File

const val RESULT_PHOTO = 1
const val PHOTO_SELECTED = 2

class GalleryFragment : Fragment() {

    private lateinit var galleryViewModel: GalleryViewModel
    private lateinit var imgPreview: ImageView

    private lateinit var recycler: RecyclerView
    private lateinit var photosHolder: PhotosHolder

    private var mCurrentPhotoPath: Uri? = null
    private var foto = byteArrayOf()

    private val photoList: MutableList<Photo> = mutableListOf()

    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        galleryViewModel = ViewModelProviders.of(this).get(GalleryViewModel::class.java)
        return inflater.inflate(R.layout.fragment_gallery, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedPreferences = activity?.getSharedPreferences("SESION", Context.MODE_PRIVATE)!!
        imgPreview = view.findViewById(R.id.imgPreview)
        recycler = view.findViewById(R.id.recycler)
        recycler.setHasFixedSize(true)
        recycler.layoutManager = GridLayoutManager(activity, 2)
        photosHolder = PhotosHolder(photoList)
        recycler.adapter = photosHolder
        view.findViewById<Button>(R.id.btnTakePhoto).setOnClickListener {
            dialogValidaFotoCarrete()
        }

        view.findViewById<Button>(R.id.btnSave).setOnClickListener {
            savePhoto()
        }

        onLoadPhotos()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        /*if (requestCode == RESULT_PHOTO) {
            if (permissions[0] == PackageManager.PERMISSION_GRANTED) {

            }
        }*/
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RESULT_PHOTO) {
            val b: Bitmap = ImageUtil.ponerFoto(context, imgPreview, mCurrentPhotoPath)
            if (b != null) {
                val bitmap = if (b.width > b.height) Bitmap.createScaledBitmap(b, 600, 400, true)
                    else Bitmap.createScaledBitmap(b, 400, 600, true)
                val bits = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bits)
                foto = bits.toByteArray()
            }
        }

        if (requestCode == PHOTO_SELECTED) {
            if (data != null) {
                val imageuri = data.data
                val b = ImageUtil.ponerFoto(activity, imgPreview, imageuri)
                if (b != null) {
                    val bitmap = if (b.width > b.height) Bitmap.createScaledBitmap(b, 600, 400, true)
                        else Bitmap.createScaledBitmap(b, 400, 600, true)
                    //ImageUtil.rotateImage(bitmap, 90);
                    val baos = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                    foto = baos.toByteArray()
                }
            }
        }
    }

    private fun takePhoto() {
        if (ContextCompat.checkSelfPermission(activity as Main2Activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
            && ContextCompat.checkSelfPermission(activity as Main2Activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity as Main2Activity, Manifest.permission.CAMERA)) {
                Snackbar.make(imgPreview, "Sín el permiso no puedo hacer uso de la camara",
                    Snackbar.LENGTH_INDEFINITE).setAction("Entendido") { v: View? ->
                    ActivityCompat.requestPermissions(activity as Main2Activity, arrayOf(Manifest.permission.CAMERA), RESULT_PHOTO)
                }.show()
            } else {
                ActivityCompat.requestPermissions(activity as Main2Activity,
                    arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE),
                    RESULT_PHOTO)
            }
        } else if (ContextCompat.checkSelfPermission(activity as Main2Activity, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            try {
                context?.let {
                    val imgFile: File = FileUtil.createImageFile(it)
                    val photoURI = FileProvider.getUriForFile(it, (activity as Main2Activity).packageName + ".fileprovider", imgFile)
                    mCurrentPhotoPath = photoURI
                    val i = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    i.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    (activity as Main2Activity).setResult(RESULT_PHOTO, i)
                    startActivityForResult(i, RESULT_PHOTO)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun dialogValidaFotoCarrete() {
        AlertDialog.Builder(activity as Main2Activity)
            .setTitle("TECGURUS").setMessage("Seleccionar imagen de")
            .setPositiveButton("Fotografía") { _, _ ->
                 takePhoto()
            }.setNegativeButton("Galería") { _, _ ->
                val i = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
                i.type = "image/*"
                startActivityForResult(Intent.createChooser(i, "Seleccione una imagen"), PHOTO_SELECTED)
            }.setNeutralButton("Cancelar", null).setCancelable(false).create().show()
    }



    fun savePhoto() {
        MyProgressDialog.showLoader(activity as Main2Activity)
        if (foto.isNotEmpty()) {
            val metadata = StorageMetadata.Builder().setContentType("image/jpeg").build()

            val ref = FirebaseDatabase.getInstance().getReference("Photos")
            val key: String = ref.push().key.toString()
            val imgRef = FirebaseStorage.getInstance().getReference("Photos")
                .child("$key.jpeg")
            val uploadTask = imgRef.putBytes(foto, metadata)
            uploadTask.addOnSuccessListener {
                MyProgressDialog.hideLoader()
                MyToast.message(activity as Main2Activity, "se ha guardado")
                foto = byteArrayOf()

                var ref2 = FirebaseDatabase.getInstance().getReference("Photos")
                ref2 = FirebaseDatabase.getInstance().getReference("Photos").child(ref2.push().key.toString())

                ref2.setValue(Photo("", imgRef.path, sharedPreferences.getString("EMAIL", "").toString(), sharedPreferences.getString("KEY_USER", "").toString()).toMap())

            }.addOnFailureListener {
                MyProgressDialog.hideLoader()
                MyToast.message(activity as Main2Activity, it.message.toString())
            }
        } else MyToast.message(activity as Main2Activity, "Selecciona una foto")
    }

    private fun onLoadPhotos() {
        MyProgressDialog.showLoader(activity as Main2Activity)
         val ref = FirebaseDatabase.getInstance().getReference("Photos")
        ref.addValueEventListener(object: ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(p0: DataSnapshot) {
                photoList.clear()
                photosHolder.notifyDataSetChanged()
                for (data in p0.children) {
                    val ref2 = FirebaseDatabase.getInstance().getReference("Account").child(data.child("KEY_USER").value.toString())
                    ref2.addValueEventListener(object: ValueEventListener {
                        override fun onDataChange(p1: DataSnapshot) {
                            photoList.add(Photo(data.key.toString(), data.child("URL").value.toString(), p1.child("Correo").value.toString(), p1.key.toString()))
                            photosHolder.notifyDataSetChanged()
                        }

                        override fun onCancelled(p0: DatabaseError) {

                        }
                    })
                }
            }
        })
        MyProgressDialog.hideLoader()
    }
}