package com.example.appfinaltecgurus.webservice;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

public class LabelDescripcionList implements KvmSerializable {
    private DRAlternativeInfo drAlternativeInfo = new DRAlternativeInfo();
    private String aditionalInfo = "NA";
    private String content = "";
    private String contentDescripcion = "";
    private String costCenter = "0";
    private boolean deliverytoEastafetaOffice = false;
    private String destinationCountryId = "MX";
    private DestinationInfo destinationInfo = new DestinationInfo();
    private int numberOfLabels = 1;
    private String officeNum = "";
    private OriginInfo originInfo = new OriginInfo();
    private String originZipCodeForRouting = "";
    private int parcelTypeId = 4;
    private String reference = "ND";
    private boolean returnDocument = false;
    private String serviceTypeId = "70";
    private boolean valid = true;
    private String weight = "";

    public DRAlternativeInfo getDrAlternativeInfo() { return drAlternativeInfo; }

    public void setDrAlternativeInfo(DRAlternativeInfo drAlternativeInfo) { this.drAlternativeInfo = drAlternativeInfo; }

    public String getAditionalInfo() { return aditionalInfo; }

    public void setAditionalInfo(String aditionalInfo) { this.aditionalInfo = aditionalInfo; }

    public String getContent() { return content; }

    public void setContent(String content) { this.content = content; }

    public String getContentDescripcion() { return contentDescripcion; }

    public void setContentDescripcion(String contentDescripcion) { this.contentDescripcion = contentDescripcion; }

    public String getCostCenter() { return costCenter; }

    public void setCostCenter(String costCenter) { this.costCenter = costCenter; }

    public boolean isDeliverytoEastafetaOffice() { return deliverytoEastafetaOffice; }

    public void setDeliverytoEastafetaOffice(boolean deliverytoEastafetaOffice) { this.deliverytoEastafetaOffice = deliverytoEastafetaOffice; }

    public String getDestinationCountryId() { return destinationCountryId; }

    public void setDestinationCountryId(String destinationCountryId) { this.destinationCountryId = destinationCountryId; }

    public DestinationInfo getDestinationInfo() { return destinationInfo; }

    public void setDestinationInfo(DestinationInfo destinationInfo) { this.destinationInfo = destinationInfo; }

    public int getNumberOfLabels() { return numberOfLabels; }

    public void setNumberOfLabels(int numberOfLabels) { this.numberOfLabels = numberOfLabels; }

    public String getOfficeNum() { return officeNum; }

    public void setOfficeNum(String officeNum) { this.officeNum = officeNum; }

    public OriginInfo getOriginInfo() { return originInfo; }

    public void setOriginInfo(OriginInfo originInfo) { this.originInfo = originInfo; }

    public String getOriginZipCodeForRouting() { return originZipCodeForRouting; }

    public void setOriginZipCodeForRouting(String originZipCodeForRouting) { this.originZipCodeForRouting = originZipCodeForRouting; }

    public int getParcelTypeId() { return parcelTypeId; }

    public void setParcelTypeId(int parcelTypeId) { this.parcelTypeId = parcelTypeId; }

    public String getReference() { return reference; }

    public void setReference(String reference) { this.reference = reference; }

    public boolean isReturnDocument() { return returnDocument; }

    public void setReturnDocument(boolean returnDocument) { this.returnDocument = returnDocument; }

    public String getServiceTypeId() { return serviceTypeId; }

    public void setServiceTypeId(String serviceTypeId) { this.serviceTypeId = serviceTypeId; }

    public boolean isValid() { return valid; }

    public void setValid(boolean valid) { this.valid = valid; }

    public String getWeight() { return weight; }

    public void setWeight(String weight) { this.weight = weight; }

    @Override
    public Object getProperty(int i) {
        switch (i) {
            case 0: return drAlternativeInfo;
            case 1: return aditionalInfo;
            case 2: return content;
            case 3: return contentDescripcion;
            case 4: return costCenter;
            case 5: return deliverytoEastafetaOffice;
            case 6: return destinationCountryId;
            case 7: return destinationInfo;
            case 8: return numberOfLabels;
            case 9: return officeNum;
            case 10: return originInfo;
            case 11: return originZipCodeForRouting;
            case 12: return parcelTypeId;
            case 13: return reference;
            case 14: return returnDocument;
            case 15: return serviceTypeId;
            case 16: return valid;
            case 17: return weight;
        }
        return null;
    }

    @Override
    public int getPropertyCount() { return 18; }

    @Override
    public void setProperty(int i, Object o) { }

    @Override
    public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
        switch (i) {
            case 0:
                propertyInfo.type = DRAlternativeInfo.class;
                propertyInfo.name = "DRAlternativeInfo";
                break;
            case 1:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "aditionalInfo";
                break;
            case 2:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "content";
                break;
            case 3:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "contentDescription";
                break;
            case 4:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "costCenter";
                break;
            case 5:
                propertyInfo.type = PropertyInfo.BOOLEAN_CLASS;
                propertyInfo.name = "deliveryToEstafetaOffice";
                break;
            case 6:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "destinationCountryId";
                break;
            case 7:
                propertyInfo.type = DestinationInfo.class;
                propertyInfo.name = "destinationInfo";
                break;
            case 8:
                propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                propertyInfo.name = "numberOfLabels";
                break;
            case 9:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "officeNum";
                break;
            case 10:
                propertyInfo.type = OriginInfo.class;
                propertyInfo.name = "originInfo";
                break;
            case 11:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "originZipCodeForRouting";
                break;
            case 12:
                propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                propertyInfo.name = "parcelTypeId";
                break;
            case 13:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "reference";
                break;
            case 14:
                propertyInfo.type = PropertyInfo.BOOLEAN_CLASS;
                propertyInfo.name = "returnDocument";
                break;
            case 15:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "serviceTypeId";
                break;
            case 16:
                propertyInfo.type = PropertyInfo.BOOLEAN_CLASS;
                propertyInfo.name = "valid";
                break;
            case 17:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "weight";
                break;
        }
    }
}
