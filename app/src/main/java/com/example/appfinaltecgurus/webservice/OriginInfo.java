package com.example.appfinaltecgurus.webservice;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

public class OriginInfo implements KvmSerializable {

    private String address1 = "";
    private String address2 = "";
    private String cellphone = "";
    private String city = "";
    private String contactName = "";
    private String corporateName  = "";
    private String customerNumber = "";
    private String neighborhood = "";
    private String phoneNumber = "";
    private String state = "";
    private boolean valid = true;
    private String zipCode = "";

    private String calle;
    private String numInt;
    private String numExt;
    private String pais;

    OriginInfo() {}

    public String getAddress1() { return address1; }

    public void setAddress1(String address1) { this.address1 = address1; }

    public String getAddress2() { return address2; }

    public void setAddress2(String address2) { this.address2 = address2; }

    public String getCellphone() { return cellphone; }

    public void setCellphone(String cellphone) { this.cellphone = cellphone; }

    public String getCity() { return city; }

    public void setCity(String city) { this.city = city; }

    public String getContactName() { return contactName; }

    public void setContactName(String contactName) { this.contactName = contactName; }

    public String getCorporateName() { return corporateName; }

    public void setCorporateName(String corporateName) { this.corporateName = corporateName; }

    public String getCustomerNumber() { return customerNumber; }

    public void setCustomerNumber(String customerNumber) { this.customerNumber = customerNumber; }

    public String getNeighborhood() { return neighborhood; }

    public void setNeighborhood(String neighborhood) { this.neighborhood = neighborhood; }

    public String getPhoneNumber() { return phoneNumber; }

    public void setPhoneNumber(String phoneNumber) { this.phoneNumber = phoneNumber; }

    public String getState() { return state; }

    public void setState(String state) { this.state = state; }

    public boolean isValid() { return valid; }

    public void setValid(boolean valid) { this.valid = valid; }

    public String getZipCode() { return zipCode; }

    public void setZipCode(String zipCode) { this.zipCode = zipCode; }

    public String getNumInt() { return numInt; }

    public String getNumExt() { return numExt; }

    public String getCalle() { return calle; }

    public String getPais() { return pais; }

    @Override
    public Object getProperty(int i) {
        switch (i) {
            case 0: return address1.trim();
            case 1: return address2.trim();
            case 2: return cellphone.trim();
            case 3: return city.trim();
            case 4: return contactName.trim();
            case 5: return corporateName.trim();
            case 6: return customerNumber.trim();
            case 7: return neighborhood.trim();
            case 8: return phoneNumber.trim();
            case 9: return state.trim();
            case 10: return valid;
            case 11: return zipCode.trim();
        }
        return null;
    }

    @Override
    public int getPropertyCount() { return 12; }

    @Override
    public void setProperty(int i, Object o) { }

    @Override
    public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
        switch (i) {
            case 0:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "address1";
                break;
            case 1:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "address2";
                break;
            case 2:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "cellPhone";
                break;
            case 3:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "city";
                break;
            case 4:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "contactName";
                break;
            case 5:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "corporateName";
                break;
            case 6:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "customerNumber";
                break;
            case 7:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "neighborhood";
                break;
            case 8:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "phoneNumber";
                break;
            case 9:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "state";
                break;
            case 10:
                propertyInfo.type = PropertyInfo.BOOLEAN_CLASS;
                propertyInfo.name = "valid";
                break;
            case 11:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "zipCode";
                break;
        }
    }
}
