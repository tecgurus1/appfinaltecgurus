package com.example.appfinaltecgurus.ui.slideshow

data class ContactData(var id: Int, var name: String) {
    var numberList: ArrayList<NumberContact> = ArrayList()

    class NumberContact(var id: Int = 0, var number: String = "") {
        var upgrade: Boolean = false
        var upgraded: Boolean = false
        var newNumber: String = ""
    }
}