package com.example.appfinaltecgurus.ui.tools

import android.app.ActivityOptions
import android.content.Intent
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.appfinaltecgurus.Main2Activity
import com.example.appfinaltecgurus.Main3Activity
import com.example.appfinaltecgurus.R


class ToolsFragment : Fragment() {

    private lateinit var toolsViewModel: ToolsViewModel

    private lateinit var testAnimation: AnimationDrawable
    private lateinit var imgAnim: ImageView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        toolsViewModel = ViewModelProviders.of(this).get(ToolsViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_tools, container, false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val btnScale: Button = view.findViewById(R.id.btnScale)
        btnScale.setOnClickListener {
            val anim: Animation = AnimationUtils.loadAnimation(activity, R.anim.anim_scale)
            it.startAnimation(anim)
        }

        val btnAlpha: Button = view.findViewById(R.id.btnAlpha)
        btnAlpha.setOnClickListener {
            val anim: Animation = AnimationUtils.loadAnimation(activity, R.anim.anim_alpha)
            it.startAnimation(anim)
            Handler().postDelayed({
                val animation1 = AlphaAnimation(0.2f, 1.0f)
                animation1.duration = 2000
                animation1.startOffset = 2000
                animation1.fillAfter = true
                it.startAnimation(animation1)
            }, 2000)
        }

        val btnRotate: Button = view.findViewById(R.id.btnRotate)
        btnRotate.setOnClickListener {
            val anim: Animation = AnimationUtils.loadAnimation(activity, R.anim.anim_rotate)
            it.startAnimation(anim)
        }

        val btnTranslate: Button = view.findViewById(R.id.btnTranslate)
        btnTranslate.setOnClickListener {
            val anim: Animation = AnimationUtils.loadAnimation(activity, R.anim.anim_translate)
            it.startAnimation(anim)
        }

        testAnimation = ContextCompat.getDrawable(activity as Main2Activity, R.drawable.test) as AnimationDrawable
        imgAnim = view.findViewById(R.id.imgAnim)
        imgAnim.setImageDrawable(testAnimation)
        imgAnim.setOnClickListener { testAnimation.start() }

        val imgTransition: ImageView = view.findViewById(R.id.imgTransition)
        imgTransition.setOnClickListener {
            val options0: ActivityOptions = ActivityOptions
                .makeSceneTransitionAnimation(activity, it, it.transitionName)
            (activity as Main2Activity).startActivity(Intent(activity, Main3Activity::class.java), options0.toBundle())
        }
    }
}