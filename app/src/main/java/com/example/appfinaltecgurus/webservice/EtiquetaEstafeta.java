package com.example.appfinaltecgurus.webservice;

public class EtiquetaEstafeta {
    private String pdf;
    private String GuieNumber;

    public String getPdf() { return pdf; }

    public void setPdf(String pdf) { this.pdf = pdf; }

    public String getGuieNumber() { return GuieNumber; }

    public void setGuieNumber(String guieNumber) { GuieNumber = guieNumber; }
}
