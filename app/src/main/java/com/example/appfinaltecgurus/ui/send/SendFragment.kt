package com.example.appfinaltecgurus.ui.send

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.appfinaltecgurus.Main2Activity
import com.example.appfinaltecgurus.R
import com.example.appfinaltecgurus.ScannerActivity
import com.example.appfinaltecgurus.util.QR
import com.google.android.material.snackbar.Snackbar
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.MultiFormatWriter
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel
import java.nio.charset.Charset
import java.util.*

const val RESULT_PHOTO = 23

class SendFragment : Fragment() {

    private lateinit var sendViewModel: SendViewModel

    private lateinit var v: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        sendViewModel =
            ViewModelProviders.of(this).get(SendViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_send, container, false)
        val textView: TextView = root.findViewById(R.id.text_send)
        v = root
        sendViewModel.text.observe(this, Observer {
            textView.text = it
        })
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val barcode = view.findViewById<ImageView>(R.id.barcode)
        view.findViewById<Button>(R.id.btn).setOnClickListener{
            if (ContextCompat.checkSelfPermission(activity as Main2Activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity as Main2Activity, Manifest.permission.CAMERA)) {
                    Snackbar.make(it, "Sín el permiso no puedo hacer uso de la camara",
                        Snackbar.LENGTH_INDEFINITE).setAction("Entendido") {
                        ActivityCompat.requestPermissions(activity as Main2Activity, arrayOf(Manifest.permission.CAMERA), RESULT_PHOTO)
                    }.show()
                } else {
                    ActivityCompat.requestPermissions(activity as Main2Activity,
                        arrayOf(Manifest.permission.CAMERA), RESULT_PHOTO)
                }
            } else if (ContextCompat.checkSelfPermission(activity as Main2Activity, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                startActivity(Intent(activity , ScannerActivity::class.java))
            }
        }

        val width = 800
        val height = 100
        val smallestDimension = if (width < height) 800 else 800
        val hintMap: MutableMap<EncodeHintType, ErrorCorrectionLevel> = HashMap()
        hintMap[EncodeHintType.ERROR_CORRECTION] = ErrorCorrectionLevel.H
        QR.CreateQRCode(activity as Main2Activity, barcode,"tecgurus", "ISO-8859-1", hintMap, smallestDimension, smallestDimension)//"UTF-8"
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == RESULT_PHOTO) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startActivity(Intent(activity , ScannerActivity::class.java))
            } else {
                Snackbar.make(v, "Sín el permiso no puedo hacer uso de la camara",
                    Snackbar.LENGTH_INDEFINITE).setAction("Entendido") {
                    ActivityCompat.requestPermissions(activity as Main2Activity, arrayOf(Manifest.permission.CAMERA), RESULT_PHOTO)
                }.show()
            }
        }
    }
}