package com.example.appfinaltecgurus.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.appfinaltecgurus.Main2Activity
import com.example.appfinaltecgurus.R
import com.example.appfinaltecgurus.ui.share.ShareFragment

/**
 * A placeholder fragment containing a simple view.
 */
class PlaceholderFragment : Fragment() {

    private lateinit var pageViewModel: PageViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel::class.java).apply {
            setIndex(arguments?.getInt(ARG_SECTION_NUMBER) ?: 1)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_main4, container, false)
        val textView: TextView = root.findViewById(R.id.section_label)
        pageViewModel.text.observe(this, Observer<String> {})
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val position = arguments?.get(ARG_SECTION_NUMBER).toString().toInt()
        val textView: TextView = view.findViewById(R.id.section_label)
        textView.text = (activity as Main2Activity).questions[position].question
        if ((activity as Main2Activity).questions[shareFragment.viewPager.currentItem].type == "1") {
            view.findViewById<ConstraintLayout>(R.id.open_answer).visibility = View.VISIBLE
            view.findViewById<ConstraintLayout>(R.id.open_answer).visibility = View.GONE
        } else {
            view.findViewById<ConstraintLayout>(R.id.open_answer).visibility = View.GONE
            view.findViewById<ConstraintLayout>(R.id.open_answer).visibility = View.VISIBLE
        }
    }

    companion object {
        private const val ARG_SECTION_NUMBER = "section_number"
        private lateinit var shareFragment: ShareFragment
        @JvmStatic
        fun newInstance(sectionNumber: Int, shareFragment: ShareFragment): PlaceholderFragment {
            this.shareFragment = shareFragment
            return PlaceholderFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
        }
    }
}