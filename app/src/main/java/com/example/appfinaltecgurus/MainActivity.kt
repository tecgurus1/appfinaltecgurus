package com.example.appfinaltecgurus

import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.transition.Explode
import com.example.appfinaltecgurus.dialog.MyProgressDialog
import com.example.appfinaltecgurus.dialog.MyToast
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

///keytool -list -v -keystore C:\Users\Ingenieria\Documents\AndroidStudioProjects\keys\tu.jks

class MainActivity : AppCompatActivity() {

    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val edtUser: TextInputEditText = findViewById(R.id.edtUser)
        val edtPassword: TextInputEditText = findViewById(R.id.edtPassword)

        sharedPreferences = getSharedPreferences("SESION", Context.MODE_PRIVATE)

        findViewById<Button>(R.id.btnStartSesion).setOnClickListener{
            if (edtUser.text.toString().isNotEmpty()) {
                if (edtPassword.text.toString().isNotEmpty()) {
                    MyProgressDialog.showLoader(this)
                    singIn(edtUser.text.toString(), edtPassword.text.toString())
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        FirebaseApp.initializeApp(this)

        MyProgressDialog.showLoader(context = this)
        if (sharedPreferences.contains("KEY_USER")) {
            firebaseAuth = FirebaseAuth.getInstance()
            if (firebaseAuth.currentUser != null) validateUser(firebaseAuth.currentUser!!)
            else singIn(sharedPreferences.getString("EMAIL", "").toString(), sharedPreferences.getString("PASSWORD", "").toString())
        } else MyProgressDialog.hideLoader()
    }

    fun validateUser(user: FirebaseUser) {
        if (user.uid == sharedPreferences.getString("KEY_USER", ""))
            startActivity(Intent(this, Main2Activity::class.java),
                ActivityOptions.makeSceneTransitionAnimation(this).toBundle())
        MyProgressDialog.hideLoader()
    }

    fun singIn(email: String, password: String) {
        firebaseAuth = FirebaseAuth.getInstance()
        firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener {
            if (it.isSuccessful) {
                sharedPreferences.edit().putString("KEY_USER", firebaseAuth.currentUser?.uid).apply()
                sharedPreferences.edit().putString("EMAIL", email).apply()
                sharedPreferences.edit().putString("PASSWORD", password).apply()
                startActivity(Intent(this, Main2Activity::class.java),
                    ActivityOptions.makeSceneTransitionAnimation(this).toBundle())
                MyProgressDialog.hideLoader()
            } else MyToast.message(this, "Ocurrio un error intenta de nuevo")
        }.addOnFailureListener {
            MyProgressDialog.hideLoader()
            MyToast.message(this, it.message.toString())
        }
    }
}
