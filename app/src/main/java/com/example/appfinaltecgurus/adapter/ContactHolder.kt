package com.example.appfinaltecgurus.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter
import com.afollestad.sectionedrecyclerview.SectionedViewHolder
import com.example.appfinaltecgurus.R
import com.example.appfinaltecgurus.ui.slideshow.ContactData

class ContactHolder (private val contactList: ArrayList<ContactData>): SectionedRecyclerViewAdapter<ContactHolder.ViewHolder>() {
    companion object {
        var type: Int = 1
    }
    var selectAll: Boolean = false

    override fun getItemCount(section: Int): Int = contactList[section].numberList.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder?, section: Int, relativePosition: Int, absolutePosition: Int) {
        holder?.imgCorrectNumber?.visibility = View.INVISIBLE
        holder?.chbxSelectNumber?.visibility = View.INVISIBLE

        val numberPhone = contactList[section].numberList[relativePosition]
        newFormat(numberPhone)

        if (!numberPhone.upgraded) numberPhone.upgrade = selectAll

        if (numberPhone.number.length == 10) {
            holder?.tvNumber?.text = numberPhone.number
            numberPhone.upgraded = true
            holder?.imgCorrectNumber?.visibility = View.VISIBLE
        } else if (numberPhone.number.length > 10){
            holder?.tvNumber?.text = numberPhone.number + "  ------->> ${numberPhone.newNumber}"
            holder?.chbxSelectNumber?.visibility = View.VISIBLE
            holder?.chbxSelectNumber?.isChecked = numberPhone.upgrade
            holder?.chbxSelectNumber?.setOnCheckedChangeListener { _, p1 ->
                contactList[section].numberList[relativePosition].upgrade = p1
            }
        } else {
            holder?.tvNumber?.text = holder?.itemView?.context?.resources?.getString(R.string.doesnt_mobile_number)
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onBindHeaderViewHolder(holder: ViewHolder?, section: Int, expanded: Boolean) {
        holder?.tvName?.text = "${(section + 1)}) ${contactList[section].name}"
        holder?.itemView?.setOnClickListener {
            if (isSectionExpanded(section)) {
                holder.imgPlusLess.setImageDrawable(ContextCompat.getDrawable(holder.itemView.context, android.R.drawable.ic_input_add))
                collapseSection(section)
            } else {
                holder.imgPlusLess.setImageDrawable(ContextCompat.getDrawable(holder.itemView.context, android.R.drawable.ic_delete))
                expandSection(section)
            }
        }

        if (isSectionExpanded(section)) holder?.imgPlusLess?.setImageDrawable(ContextCompat.getDrawable(holder.itemView.context, android.R.drawable.ic_delete))
        else holder?.imgPlusLess?.setImageDrawable(ContextCompat.getDrawable(holder.itemView.context, android.R.drawable.ic_input_add))
    }

    override fun getSectionCount(): Int = contactList.size

    override fun onBindFooterViewHolder(holder: ViewHolder?, section: Int) {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var resourceId = 0
        when (viewType) {
            VIEW_TYPE_HEADER -> {
                type = 1
                resourceId = R.layout.adapter_contact
            }
            VIEW_TYPE_ITEM -> {
                type = 2
                resourceId = R.layout.adapter_number_contact
            }
        }
        return ViewHolder(LayoutInflater.from(parent.context).inflate(resourceId, parent, false))
    }

    private fun newFormat(numberPhone: ContactData.NumberContact) {
        val patternMexico = Regex("""^\+521""")
        val patternMexico2 = Regex("""^\+52""")
        if (patternMexico.containsMatchIn(numberPhone.number)) {
            numberPhone.newNumber = patternMexico.replace(numberPhone.number, "")
        } else if (patternMexico2.containsMatchIn(numberPhone.number)) {
            numberPhone.newNumber = patternMexico2.replace(numberPhone.number, "")
        }
    }

    class ViewHolder(itemView: View): SectionedViewHolder(itemView) {
        lateinit var tvName: TextView
        lateinit var imgPlusLess: ImageView
        lateinit var tvNumber: TextView
        lateinit var chbxSelectNumber: CheckBox
        lateinit var imgCorrectNumber: ImageView
        init {
            if (type == 1) {
                tvName = itemView.findViewById(R.id.tvName)
                imgPlusLess = itemView.findViewById(R.id.imgPlusLess)
            } else {
                tvNumber = itemView.findViewById(R.id.tvNumber)
                chbxSelectNumber = itemView.findViewById(R.id.chbxSelectNumber)
                imgCorrectNumber = itemView.findViewById(R.id.imgCorrectNumber)
            }
        }
    }
}