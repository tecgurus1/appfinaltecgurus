package com.example.appfinaltecgurus.ui.main

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.appfinaltecgurus.model.Question
import com.example.appfinaltecgurus.ui.share.ShareFragment


/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class SectionsPagerAdapter(fm: FragmentManager, private val questions: List<Question>, var shareFragment: ShareFragment) :
    FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        /*when (questions[position].type) {
            "check" -> return PlaceholderFragment.newInstance(position)
        }*/
        return PlaceholderFragment.newInstance(position, shareFragment)
    }
    override fun getPageTitle(position: Int): CharSequence? = questions[position].question
    override fun getCount(): Int = questions.size
}