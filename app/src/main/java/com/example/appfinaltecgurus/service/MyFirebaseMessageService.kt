package com.example.appfinaltecgurus.service

import android.app.NotificationChannel
import android.app.NotificationChannelGroup
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import com.example.appfinaltecgurus.Main2Activity
import com.example.appfinaltecgurus.R
import com.example.appfinaltecgurus.dialog.MyToast
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import java.util.*

class MyFirebaseMessageService : FirebaseMessagingService(){

    private var manager: NotificationManager? = null

    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)
        manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val chanelID: String = p0.notification?.title.toString()
        val body: String = p0.notification?.body.toString()

        val i = Intent(this, Main2Activity::class.java)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

        val pendingIntent = PendingIntent.getActivity(this, 0, i, PendingIntent.FLAG_UPDATE_CURRENT)

        //val uri =
            //Uri.parse("android.resource://" + packageName + "/" + R.raw.dingdong_triple_6)

        val builder =
            NotificationCompat.Builder(this, chanelID)
                .setAutoCancel(true)
                .setGroup(chanelID)
                .setGroupSummary(true)
                .setContentTitle(chanelID)
                .setChannelId(chanelID)
                //.setDefaults(RingtoneManager.getDefaultType(uri))
                .setGroupAlertBehavior(NotificationCompat.GROUP_ALERT_SUMMARY)
               // .setSound(uri, AudioManager.STREAM_NOTIFICATION)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                //.setSound(uri)
                .setVibrate(longArrayOf(300, 700, 500, 800, 500, 900, 1000, 800, 500, 700, 300))
                .setPriority(NotificationCompat.FLAG_GROUP_SUMMARY or NotificationCompat.PRIORITY_HIGH or NotificationCompat.FLAG_INSISTENT)
                .setContentText(body)
                .setStyle(NotificationCompat.BigTextStyle().bigText(body))
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentIntent(pendingIntent)

        assert(manager != null)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel =
                    NotificationChannel(chanelID, chanelID, NotificationManager.IMPORTANCE_HIGH)
                channel.enableVibration(true)
                manager!!.createNotificationChannel(channel)
                manager!!.createNotificationChannelGroup(
                    NotificationChannelGroup(
                        chanelID,chanelID
                    )
                )
            }
        }
        manager?.notify(12, builder.build())
    }

    override fun onDeletedMessages() {
        super.onDeletedMessages()
        MyToast.message(this, "Se elimino el push")
    }

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
        val sharedPreferences = getSharedPreferences("SESION", Context.MODE_PRIVATE)
        val ref = FirebaseDatabase.getInstance().getReference("Account")
            .child(sharedPreferences.getString("KEY_USER", "").toString())
        val param = HashMap<String, Any>()
        param["TOKEN_APP"] = p0
        ref.updateChildren(param)
    }
}