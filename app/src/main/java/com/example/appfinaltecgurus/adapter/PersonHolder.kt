package com.example.appfinaltecgurus.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.appfinaltecgurus.R
import com.example.appfinaltecgurus.model.Person
import com.google.firebase.database.FirebaseDatabase

class PersonHolder(var personList: MutableList<Person>): RecyclerView.Adapter<PersonHolder.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_person, parent, false))

    override fun getItemCount(): Int = personList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val person = personList[position]
        holder.tvName.text = person.name
        holder.tvAge.text = person.age.toString()

        holder.tvDelete.setOnClickListener {
            val ref = FirebaseDatabase.getInstance().getReference("Person").child(personList[holder.adapterPosition].id)
            ref.removeValue().addOnCompleteListener {
                if (it.isSuccessful) {
                    notifyItemRemoved(holder.adapterPosition)
                }
            }
        }
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        var tvName: TextView = itemView.findViewById(R.id.tvName)
        var tvAge: TextView = itemView.findViewById(R.id.tvAge)
        var tvDelete: TextView = itemView.findViewById(R.id.tvDelete)
    }
}