package com.example.appfinaltecgurus.ui.home

interface DataLoadListener {
    fun loadPeople()
    fun cleanRecycler()
}