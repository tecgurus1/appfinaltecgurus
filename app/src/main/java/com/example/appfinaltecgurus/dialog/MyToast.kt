package com.example.appfinaltecgurus.dialog

import android.content.Context
import android.widget.Toast

class MyToast {
    companion object {
        fun message(context: Context, message: String) {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show()
        }
    }
}