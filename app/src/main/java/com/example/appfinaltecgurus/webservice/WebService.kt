package com.example.appfinaltecgurus.webservice

import android.util.Log
import org.ksoap2.SoapEnvelope
import org.ksoap2.SoapFault
import org.ksoap2.serialization.SoapObject
import org.ksoap2.serialization.SoapSerializationEnvelope
import org.ksoap2.transport.HttpTransportSE
import org.xmlpull.v1.XmlPullParserException
import java.io.IOException
import java.util.*

const val URL_Estafeta = "https://labelqa.estafeta.com/EstafetaLabel20/services/EstafetaLabelWS?wsdl"
const val timeOut = 180_000;
class WebService {

    /////////////////////////////////////////////////////////////////estafeta methods\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    @Throws(IOException::class, XmlPullParserException::class)
    fun createLabel(createLabel: CreateLabel?): EtiquetaEstafeta? {
        val soapEnvelope = SoapSerializationEnvelope(SoapEnvelope.VER11)
        soapEnvelope.implicitTypes = true
        soapEnvelope.dotNet = true
        val soapReq = SoapObject("http://dto.estafetalabel.webservices.estafeta.com", "createLabel")
        soapReq.addProperty("in0", createLabel)
        soapEnvelope.setOutputSoapObject(soapReq)
        val httpTransport = HttpTransportSE(URL_Estafeta, timeOut)
        httpTransport.debug = true
        val etiquetaEstafeta = EtiquetaEstafeta()
        httpTransport.call(
            "http://dto.estafetalabel.webservices.estafeta.com/createLabel",
            soapEnvelope
        )
        Log.d("## dump Request: ", httpTransport.requestDump)
        Log.d("## dump response: ", httpTransport.responseDump)
        val retObj = soapEnvelope.bodyIn
        if (retObj is SoapFault) {
            val ex = Exception(retObj.faultstring)
            //if (eventHandler != null) eventHandler.Wsdl2CodeFinishedWithException(ex)
        } else {
            val result = retObj as SoapObject
            if (result.propertyCount > 0) {
                val obj = result.getProperty(0) as SoapObject
                etiquetaEstafeta.setPdf(obj.getProperty(1).toString())
                val data = obj.getProperty(2) as Vector<Any>
                val num = data[0] as SoapObject
                etiquetaEstafeta.setGuieNumber(num.getProperty(1).toString())
            }
        }
        return etiquetaEstafeta
    }
    /*
    <in0 xsi:type="dto:EstafetaLabelRequest" xmlns:dto="http://dto.estafetalabel.webservices.estafeta.com">
            <customerNumber xsi:type="xsd:string">0000000</customerNumber>
            <!--1 or more repetitions:-->
            <labelDescriptionList xsi:type="dto:LabelDescriptionList">
               <DRAlternativeInfo xsi:type="dto:DRAlternativeInfo">
                  <address1 xsi:type="xsd:string">CERRADA DE CEYLAN</address1>
                  <address2 xsi:type="xsd:string">539</address2>
                  <cellPhone xsi:type="xsd:string">55555555</cellPhone>
                  <city xsi:type="xsd:string">AZCAPOTZALCO</city>
                  <contactName xsi:type="xsd:string">CARLOS MATEOS</contactName>
                  <corporateName xsi:type="xsd:string">INTERNET SA  DE CV</corporateName>
                  <customerNumber xsi:type="xsd:string">0000000</customerNumber>
                  <neighborhood xsi:type="xsd:string">INDUSTRIAL</neighborhood>
                  <phoneNumber xsi:type="xsd:string">6666666666</phoneNumber>
                  <state xsi:type="xsd:string">DF</state>
                  <valid xsi:type="xsd:boolean">True</valid>
                  <zipCode xsi:type="xsd:string">02300</zipCode>
               </DRAlternativeInfo>
               <aditionalInfo xsi:type="xsd:string">OPERACION5</aditionalInfo>
               <content xsi:type="xsd:string">JOYAS</content>
               <contentDescription xsi:type="xsd:string">ORO</contentDescription>
               <costCenter xsi:type="xsd:string">12345</costCenter>
               <deliveryToEstafetaOffice xsi:type="xsd:boolean">False</deliveryToEstafetaOffice>
               <destinationCountryId xsi:type="xsd:string">MX</destinationCountryId>
               <destinationInfo xsi:type="dto:DestinationInfo">
                  <address1 xsi:type="xsd:string">MAIZALES</address1>
                  <address2 xsi:type="xsd:string">35</address2>
                  <cellPhone xsi:type="xsd:string">4444444</cellPhone>
                  <city xsi:type="xsd:string">COYOACAN</city>
                  <contactName xsi:type="xsd:string">JAVIER SANCHEZ</contactName>
                  <corporateName xsi:type="xsd:string">CHICOLOAPAN SA DE CV</corporateName>
                  <customerNumber xsi:type="xsd:string">0000000</customerNumber>
                  <neighborhood xsi:type="xsd:string">CENTRO</neighborhood>
                  <phoneNumber xsi:type="xsd:string">777777</phoneNumber>
                  <state xsi:type="xsd:string">ESTADO  DE MEXICO</state>
                  <valid xsi:type="xsd:boolean">True</valid>
                  <zipCode xsi:type="xsd:string">02130</zipCode>
               </destinationInfo>
               <numberOfLabels xsi:type="xsd:int">1</numberOfLabels>
               <officeNum xsi:type="xsd:string">130</officeNum>
               <originInfo xsi:type="dto:OriginInfo">
                  <address1 xsi:type="xsd:string">CALLE 5</address1>
                  <address2 xsi:type="xsd:string">29</address2>
                  <cellPhone xsi:type="xsd:string">888888</cellPhone>
                  <city xsi:type="xsd:string">TLALPAN</city>
                  <contactName xsi:type="xsd:string">JANET OIDOR</contactName>
                  <corporateName xsi:type="xsd:string">ALTAS SA DE CV</corporateName>
                  <customerNumber xsi:type="xsd:string">0000000</customerNumber>
                  <neighborhood xsi:type="xsd:string">CENTRO</neighborhood>
                  <phoneNumber xsi:type="xsd:string">9999999</phoneNumber>
                  <state xsi:type="xsd:string">DF</state>
                  <valid xsi:type="xsd:boolean">True</valid>
                  <zipCode xsi:type="xsd:string">02300</zipCode>
               </originInfo>
               <originZipCodeForRouting xsi:type="xsd:string">02300</originZipCodeForRouting>
               <parcelTypeId xsi:type="xsd:int">1</parcelTypeId>
               <reference xsi:type="xsd:string">FRENTE AL SANBORNS</reference>
               <returnDocument xsi:type="xsd:boolean">false</returnDocument>
               <serviceTypeId xsi:type="xsd:string">70</serviceTypeId>
               <valid xsi:type="xsd:boolean">true</valid>
               <weight xsi:type="xsd:float">5</weight>
            </labelDescriptionList>
            <labelDescriptionListCount xsi:type="xsd:int">1</labelDescriptionListCount>
            <login xsi:type="xsd:string">prueba1</login>
            <paperType xsi:type="xsd:int">1</paperType>
            <password xsi:type="xsd:string">lAbeL_K_11</password>
            <quadrant xsi:type="xsd:int">0</quadrant>
            <suscriberId xsi:type="xsd:string">28</suscriberId>
            <valid xsi:type="xsd:boolean">True</valid>
         </in0>
    * */
}