package com.example.appfinaltecgurus.adapter

import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.appfinaltecgurus.R
import com.example.appfinaltecgurus.dialog.MyToast
import com.example.appfinaltecgurus.model.Photo
import com.example.appfinaltecgurus.util.FileUtil
import com.example.appfinaltecgurus.util.ImageUtil
import com.google.firebase.storage.FirebaseStorage
import java.io.File

const val ONE_MEGABYTE: Long = 1024 * 1024
class PhotosHolder(var photoList: MutableList<Photo>) : RecyclerView.Adapter<PhotosHolder.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_photo, parent, false))

    override fun getItemCount(): Int = photoList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val photo = photoList[position]
        holder.tvWhoTook.text = photo.photogapher
        val storageReference = FirebaseStorage.getInstance().reference.child(photo.url)
        /*Glide.with(holder.imgPhoto.context)
            .load(storageReference)
            .into(holder.imgPhoto)*/

        //storageReference = FirebaseStorage.getInstance().getReference(photo.url)
        storageReference.getBytes(ONE_MEGABYTE).addOnSuccessListener { bytes: ByteArray ->
            if (bytes.isNotEmpty()) {
                ImageUtil.setByteToImageView(holder.imgPhoto, bytes, R.mipmap.ic_launcher)
                val archivo: File = FileUtil.saveFile(holder.imgPhoto.context, bytes, Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "tecgurus$position.jpeg")
                if (archivo.exists()) MyToast.message(holder.imgPhoto.context, "Se descargo correctamente")
                else MyToast.message(holder.imgPhoto.context, "Algo salio mal")
            }
        }.addOnFailureListener { exception: Exception ->
            Log.e("SínFoto", exception.message.toString())
        }
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val imgPhoto = itemView.findViewById<ImageView>(R.id.imgPhoto)
        val tvWhoTook = itemView.findViewById<TextView>(R.id.tvWhoTook)
    }
}