package com.example.appfinaltecgurus.model

data class Photo(var id: String, var url: String, var photogapher: String, var idPhotogapher: String) {

    fun toMap(): Map<String, Any> {
        val param = mutableMapOf<String, Any>()
        param["URL"] = url
        param["KEY_USER"] = idPhotogapher
        return param
    }
}