package com.example.appfinaltecgurus.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.appfinaltecgurus.model.Person

class PersonListViewModel: ViewModel() {

    private var personList: MutableLiveData<MutableList<Person>>? = null

    fun getPersonList(): LiveData<MutableList<Person>> {
        if (personList == null) {
            personList = MutableLiveData()
        }
        return personList as LiveData<MutableList<Person>>
    }

    fun init(context: DataLoadListener) {
        if (personList != null) return
        personList = PersonListRespository.getInstance(context)?.getPeople()
    }
}

