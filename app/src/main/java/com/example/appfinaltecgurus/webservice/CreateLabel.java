package com.example.appfinaltecgurus.webservice;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

public class CreateLabel implements KvmSerializable {
    private LabelDescripcionList labelDescripcionList;
    private String customerNumber;
    private int labelDescripcionListCount;
    private String login;
    private String password;
    private String suscriberId;
    private int paperType = 2;
    private int quadrant = 0;
    private boolean valid = true;

    public LabelDescripcionList getLabelDescripcionList() { return labelDescripcionList; }

    public void setLabelDescripcionList(LabelDescripcionList labelDescripcionList) { this.labelDescripcionList = labelDescripcionList; }

    public String getCustomerNumber() { return customerNumber; }

    int getLabelDescripcionListCount() { return labelDescripcionListCount; }

    public void setLabelDescripcionListCount(int labelDescripcionListCount) { this.labelDescripcionListCount = labelDescripcionListCount; }

    String getLogin() { return login; }

    int getPaperType() { return paperType; }

    String getPassword() { return password; }

    int getQuadrant() { return quadrant; }

    String getSuscriberId() { return suscriberId; }

    boolean isValid() { return valid; }

    @Override
    public Object getProperty(int i) {
        switch (i) {
            case 0: return labelDescripcionList;
            case 1: return customerNumber;
            case 2: return labelDescripcionListCount;
            case 3: return login;
            case 4: return paperType;
            case 5: return password;
            case 6: return quadrant;
            case 7: return suscriberId;
            case 8: return valid;
        }
        return null;
    }

    @Override
    public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
        switch (i) {
            case 0:
                propertyInfo.type = LabelDescripcionList.class;
                propertyInfo.name = "labelDescriptionList";
                break;
            case 1:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "customerNumber";
                break;
            case 2:
                propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                propertyInfo.name = "labelDescriptionListCount";
                break;
            case 3:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "login";
                break;
            case 4:
                propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                propertyInfo.name = "paperType";
                break;
            case 5:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "password";
                break;
            case 6:
                propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                propertyInfo.name = "quadrant";
                break;
            case 7:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "suscriberId";
                break;
            case 8:
                propertyInfo.type = PropertyInfo.BOOLEAN_CLASS;
                propertyInfo.name = "valid";
                break;
        }
    }

    @Override
    public int getPropertyCount() { return 9; }

    @Override
    public void setProperty(int i, Object o) { }
}
