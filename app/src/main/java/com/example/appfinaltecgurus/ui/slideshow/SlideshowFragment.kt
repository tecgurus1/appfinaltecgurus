package com.example.appfinaltecgurus.ui.slideshow

import android.Manifest
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appfinaltecgurus.R
import com.example.appfinaltecgurus.adapter.ContactHolder

const val PERMISSION_REQUEST_READ_CONTACT = 1

class SlideshowFragment : Fragment() {
    
    private lateinit var slideshowViewModel: SlideshowViewModel
    private val contactList: ArrayList<ContactData> = ArrayList()

    private lateinit var contactAdapter: ContactHolder

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        slideshowViewModel = ViewModelProviders.of(this).get(SlideshowViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_slideshow, container, false)
        slideshowViewModel.text.observe(this, Observer {
        })
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val recycler = view.findViewById<RecyclerView>(R.id.recycler)
        recycler.setHasFixedSize(true)
        recycler.layoutManager = LinearLayoutManager(activity)
        contactAdapter = ContactHolder(contactList)
        recycler.adapter = contactAdapter

        activity?.let {
            if (ContextCompat.checkSelfPermission(it, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(it, Manifest.permission.READ_CONTACTS)) {
                    AlertDialog.Builder(activity).setTitle(activity?.resources?.getString(R.string.app_name))
                        .setMessage(activity?.resources?.getString(R.string.without_permission))
                        .setNegativeButton(android.R.string.cancel, null)
                        .setPositiveButton(android.R.string.ok) { _, _ ->
                            requestPermissions(arrayOf(Manifest.permission.READ_CONTACTS), PERMISSION_REQUEST_READ_CONTACT)
                        }.create().show()
                } else {
                    requestPermissions(arrayOf(Manifest.permission.READ_CONTACTS), PERMISSION_REQUEST_READ_CONTACT)
                }
            } else getContacts()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_REQUEST_READ_CONTACT) {
            if (grantResults.isNotEmpty()) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getContacts()
                } else {
                    AlertDialog.Builder(activity).setTitle(activity?.resources?.getString(R.string.app_name))
                        .setMessage(activity?.resources?.getString(R.string.activate_permission))
                        .setCancelable(false).setNegativeButton(android.R.string.cancel, null)
                        .setPositiveButton(android.R.string.ok) { _, _ ->
                            activity?.startActivity(
                                Intent().setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                                    .setData(Uri.fromParts("package", activity?.packageName, null)))
                        }.create().show()
                }
            }
        }
    }

    private fun getContacts() {
        val contacts = activity?.contentResolver?.query(
            ContactsContract.CommonDataKinds.Contactables.CONTENT_URI,
            null, null, null, ContactsContract.CommonDataKinds.Contactables.DISPLAY_NAME + " ASC")
        while (contacts!!.moveToNext()) {
            val idContact = contacts.getInt(contacts.getColumnIndex(ContactsContract.CommonDataKinds.Contactables.CONTACT_ID))
            val name = contacts.getString(contacts.getColumnIndex(ContactsContract.CommonDataKinds.Contactables.DISPLAY_NAME))
            print(idContact)
            print(name)
            var isExists = false
            lateinit var contactData: ContactData

            contactList.forEach {
                if (it.name == name) {
                    isExists = true
                    val phone = activity?.contentResolver?.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", arrayOf(idContact.toString()), null)//ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC")
                    while (phone!!.moveToNext()) {
                        var isExistsNum = false
                        val id = phone.getInt(phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID))
                        var number =
                            phone.getString(phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                        if (!number.contains("+")) {
                            if (number.contains("(")) number = number.replace("(", "")
                            if (number.contains(")")) number = number.replace(")", "")
                            if (number.contains(" ")) number = number.replace(" ", "")
                            if (number.contains("-")) number = number.replace("-", "")

                        } else number = number.replace(" ", "")
                        it.numberList.forEach { numb -> if (numb.number == number) isExistsNum = true }
                        if (!isExistsNum) it.numberList.add(ContactData.NumberContact(id, number))
                    }
                    phone.close()
                }
            }

            if (!isExists) {
                contactData = ContactData(idContact, name)
                val phone = activity?.contentResolver?.query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", arrayOf(idContact.toString()),null)//ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC")
                while (phone!!.moveToNext()) {
                    val numbers = contactData.numberList
                    val id = phone.getInt(phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID))
                    var number = phone.getString(phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                    if (!number.contains("+")) {
                        if (number.contains("(")) number = number.replace("(", "")
                        if (number.contains(")")) number = number.replace(")", "")
                        if (number.contains(" ")) number = number.replace(" ", "")
                        if (number.contains("-")) number = number.replace("-", "")
                    } else number = number.replace(" ", "")
                    numbers.add(ContactData.NumberContact(id, number))
                }
                phone.close()
                if (contactData.numberList.size > 0) contactList.add(contactData)
            }
        }
        contacts.close()
        contactAdapter.notifyDataSetChanged()
    }
}