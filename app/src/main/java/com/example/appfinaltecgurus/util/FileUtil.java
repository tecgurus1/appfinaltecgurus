package com.example.appfinaltecgurus.util;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class FileUtil {

    public static File createImageFile(Context context) throws IOException {
        String imageFileName = "JPEG_" + System.currentTimeMillis() + "_";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        //mCurrentPhotoPath = image.getAbsolutePath();
        return File.createTempFile(imageFileName, ".jpg", storageDir);
    }

    public static File saveFile(Context context, byte[] fileBytes, File archivoDestino, String nombreDoc) {
        try {
            Log.e("root", archivoDestino.getAbsolutePath());
            File file = new File(archivoDestino, nombreDoc);
            OutputStream out = new FileOutputStream(file);
            out.write(fileBytes);
            out.close();
            MediaScannerConnection.scanFile(context, new String[] { file.getPath() },
                    null, (path, uri) -> Log.e("FILES", "Scan finished. You can view the file on downloads."));
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("escribirArchivo", e.getMessage());
        }
        return archivoDestino;
    }
}
