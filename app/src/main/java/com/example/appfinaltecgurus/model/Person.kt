package com.example.appfinaltecgurus.model

data class Person(var id: String, var name: String, var age: Int) {

    fun toMap(): Map<String, Any> {
        val params: MutableMap<String, Any> = mutableMapOf()
        params["Name"] = name
        params["Age"] = age
        return params
    }
}