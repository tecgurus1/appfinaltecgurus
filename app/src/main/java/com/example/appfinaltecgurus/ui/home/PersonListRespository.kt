package com.example.appfinaltecgurus.ui.home

import androidx.lifecycle.MutableLiveData
import com.example.appfinaltecgurus.model.Person
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class PersonListRespository {

    private val personList: MutableList<Person> = mutableListOf()

    companion object {
        private lateinit var dataLoadListener: DataLoadListener
        private var instance: PersonListRespository? = null

        fun getInstance(context: DataLoadListener): PersonListRespository? {
            if (instance == null) instance = PersonListRespository()
            dataLoadListener = context
            return instance
        }
    }

    fun getPeople(): MutableLiveData<MutableList<Person>> {
        loadPeople()

        val people: MutableLiveData<MutableList<Person>> = MutableLiveData()
        people.value = personList

        return people
    }

    private fun loadPeople() {
        personList.clear()
        val ref = FirebaseDatabase.getInstance().reference
        val query = ref.child("Person")
        query.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                personList.clear()
                dataLoadListener.cleanRecycler()
                for (data in p0.children) {
                    personList.add(Person(data.key.toString(), data.child("Name").value.toString(), data.child("Age").value.toString().toInt()))
                }
                dataLoadListener.loadPeople()
            }

            override fun onCancelled(p0: DatabaseError) {

            }
        })
    }
}