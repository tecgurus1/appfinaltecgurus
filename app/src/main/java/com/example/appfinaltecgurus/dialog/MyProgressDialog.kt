package com.example.appfinaltecgurus.dialog

import android.app.ProgressDialog
import android.content.Context

class MyProgressDialog {

    companion object {
        private lateinit var progressDialog: ProgressDialog

        fun showLoader(context: Context) {
            progressDialog = ProgressDialog(context)
            progressDialog.setTitle("Tecgusurs")
            progressDialog.setCancelable(false)
            progressDialog.setMessage("Validando...")
            progressDialog.show()
        }

        fun hideLoader() {
            if (progressDialog.isShowing) progressDialog.dismiss()
        }
    }
}