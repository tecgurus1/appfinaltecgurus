package com.example.appfinaltecgurus.ui.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.findNavController
import com.example.appfinaltecgurus.R
import com.example.appfinaltecgurus.model.Person
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.database.FirebaseDatabase

/**
 * A simple [Fragment] subclass.
 */
class DataPerson : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_data_person, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val edtName: TextInputEditText = view.findViewById(R.id.edtName)
        val edtAge: TextInputEditText = view.findViewById(R.id.edtAge)

        view.findViewById<Button>(R.id.btnSave).setOnClickListener {
            if (edtName.text.toString().isNotEmpty()) {
                if (edtAge.text.toString().isNotEmpty()) {
                    val person = Person("",edtName.text.toString(), edtAge.text.toString().toInt())
                    var ref = FirebaseDatabase.getInstance().getReference("Person")
                    val key = ref.push().key
                    ref = FirebaseDatabase.getInstance().getReference("Person").child(key.toString())
                    ref.setValue(person.toMap()).addOnCompleteListener {
                        if (it.isSuccessful) view.findNavController().navigate(R.id.action_dataPerson_to_personList)
                    }
                }
            }
        }
    }
}
