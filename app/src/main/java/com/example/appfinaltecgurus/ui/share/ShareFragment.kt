package com.example.appfinaltecgurus.ui.share

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.example.appfinaltecgurus.Main2Activity
import com.example.appfinaltecgurus.R
import com.example.appfinaltecgurus.ui.main.SectionsPagerAdapter

class ShareFragment : Fragment(), View.OnClickListener {

    private lateinit var shareViewModel: ShareViewModel

    lateinit var btnBack: Button
    lateinit var btnNext: Button
    lateinit var viewPager: ViewPager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        shareViewModel = ViewModelProviders.of(this).get(ShareViewModel::class.java)
        return inflater.inflate(R.layout.fragment_share, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val sectionsPagerAdapter = fragmentManager?.let { SectionsPagerAdapter(it, (activity as Main2Activity).questions, this) }
        viewPager = view.findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter

        btnBack = view.findViewById(R.id.btnBack)
        btnNext = view.findViewById(R.id.btnNext)

        btnBack.setOnClickListener(this)
        btnNext.setOnClickListener(this)
        visibleButtons()
    }


    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.btnNext ->  {
                if (viewPager.currentItem == (activity as Main2Activity).questions.size) return
                viewPager.setCurrentItem(viewPager.currentItem + 1, true)
                visibleButtons()
            }
            R.id.btnBack ->  {
                viewPager.setCurrentItem(viewPager.currentItem - 1, true)
                visibleButtons()
            }
        }
    }

     private fun visibleButtons() {
         if (viewPager.currentItem == 0) btnBack.visibility = View.GONE
         else btnBack.visibility = View.VISIBLE

         if (viewPager.currentItem == (activity as Main2Activity).questions.size) btnNext.text = "Finalizar"
         else btnNext.text = "Siguiente"
     }
}