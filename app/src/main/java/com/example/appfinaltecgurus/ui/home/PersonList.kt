package com.example.appfinaltecgurus.ui.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appfinaltecgurus.Main2Activity
import com.example.appfinaltecgurus.R
import com.example.appfinaltecgurus.adapter.PersonHolder
import com.example.appfinaltecgurus.dialog.MyToast
import com.example.appfinaltecgurus.model.Person
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.database.*

/**
 * A simple [Fragment] subclass.
 */
class PersonList : Fragment(), DataLoadListener {

    private lateinit var recycler: RecyclerView

    private lateinit var personHolder: PersonHolder
    //private val personList: MutableList<Person> = mutableListOf()

    private lateinit var personListViewModel: PersonListViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_person_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recycler = view.findViewById(R.id.recycler)
        recycler.setHasFixedSize(true)
        recycler.layoutManager = LinearLayoutManager(activity)

        //onCharger()
        personListViewModel = ViewModelProviders.of(activity as Main2Activity).get(PersonListViewModel::class.java)
        personListViewModel.init(this@PersonList)
        personHolder = PersonHolder(personListViewModel.getPersonList().value as MutableList<Person>)
        recycler.adapter = personHolder
        personHolder.notifyDataSetChanged()

        view.findViewById<FloatingActionButton>(R.id.fabAddPerson).setOnClickListener {
            it.findNavController().navigate(R.id.action_personList_to_dataPerson)
        }
    }

    override fun loadPeople() {
        personListViewModel.getPersonList().observe(this, Observer {
            //personList.clear()
            //personHolder.notifyDataSetChanged()
            //personList.addAll(it)
            personHolder.notifyDataSetChanged()
        })
    }

    override fun cleanRecycler() {
        personHolder.notifyDataSetChanged()
    }

    /*private fun onCharger() {
        personList.clear()
        val ref = FirebaseDatabase.getInstance().getReference("Person")
        ref.addValueEventListener(object: ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
                activity?.let { MyToast.message(it, p0.message) }
            }

            override fun onDataChange(p0: DataSnapshot) {
                personList.clear()
                personHolder.notifyDataSetChanged()
                for (data in p0.children) {
                    personList.add(Person(data.key.toString(), data.child("Name").value.toString(), data.child("Age").value.toString().toInt()))
                }
                personHolder.notifyDataSetChanged()
            }
        })
    }*/
}
